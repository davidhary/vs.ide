# Visual Studio IDE Shared Files

Includes common code snippets for visual studio IDE.

* [Source Code](#Source-Code)
* [MIT License](LICENSE.md)
* [Change Log](CHANGELOG.md)
* [Facilitated By](#FacilitatedBy)
* [Authors](#Authors)
* [Acknowledgments](#Acknowledgments)
* [Open Source](#Open-Source)
* [Closed Software](#Closed-software)

<a name="Source-Code"></a>
## Source Code
Clone the repository along with its requisite repositories to their respective relative path.

### Repositories
The repositories listed in [external repositories](ExternalReposCommits.csv) are required:
* [IDE](https://www.bitbucket.org/davidhary/vs.ide) - IDE Shared Files

```
git clone git@bitbucket.org:davidhary/vs.ide.git
```

Clone the repositories into the following folders (parents of the .git folder):
```
.\Libraries\VS\Core\IDE
```

<a name="FacilitatedBy"></a>
## Facilitated By
* [Visual Studio](https://www.visualstudio.com/) - Visual Studio
* [Jarte](https://www.jarte.com/) - RTF Editor
* [Wix Installer](https://www.wixtoolset.org/) - WiX Toolset
* [Atomineer Code Documentation](https://www.atomineerutils.com/) - Code Documentation
* [EW Software](https://github.com/EWSoftware/VSSpellChecker/wiki/) - Spell Checker
* [Code Converter](https://github.com/icsharpcode/CodeConverter) - Code Converter
* [Search and Replace](http://www.funduc.com/search_replace.htm) - Funduc Search and Replace for Windows

<a name="Authors"></a>
## Authors
* [ATE Coder](https://www.IntegratedScientificResources.com)

<a name="Acknowledgments"></a>
## Acknowledgments
* [Its all a remix](https://www.everythingisaremix.info) -- we are but a spec on the shoulders of giants
* [John Simmons](https://www.codeproject.com/script/Membership/View.aspx?mid=7741) - outlaw programmer
* [Stack overflow](https://www.stackoveflow.com) - Joel Spolsky

<a name="Open-Source"></a>
### Open source
Open source used by this software is described and licensed at the
following sites:  
[Core Libraries](https://bitbucket.org/davidhary/vs.core)

<a name="Closed-software"></a>
### Closed software
Closed software used by this software are described and licensed on
the following sites:  
[Core Libraries](https://bitbucket.org/davidhary/vs.core)

