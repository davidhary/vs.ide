### Changelog
All notable changes to these libraries will be documented in this file.
The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)

#### [x.x.8940] - 2024-06-20
* Namespace: remove cc.isr prefix requirement.
* add constant rule, which works sometimes.

#### [x.x.8939] - 2024-06-19
* Update editor config for using the cc.isr prefix. this fails on some but not all classes.

#### [x.x.8934] - 2024-06-15
* Update editor config fixing code style and constants.

#### [x.x.8931] - 2024-06-13
* Update editor config per https://github.com/RehanSaeed

#### [x.x.7616] - 2020-11-06
* Moved from the [Share Repository](https://www.bitbucket.org/davidhary/vs.share)

\(C\) 2010 Integrated Scientific Resources, Inc. All rights reserved.
[x.x.8939]: https://bitbucket.org/davidhary/vs.ide/src/main/